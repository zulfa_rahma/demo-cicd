pipeline {
    agent any

    environment {
        APP_NAME = "cicd-dodol"
        PUBLISH_PORT = 8228
        DOCKER_HUB_REPO = "bluepaintedwall/demo-cicd"
    }

    stages {
        stage('Build using Maven') {
            steps {
                script{
                    sh """
                    export PATH=$PATH:/mnt/apache-maven-3.9.6/bin
                    mvn clean package
                    """
                }
            }   
        }

        stage('Code Analysis') {
        environment{
            SCANNER_HOME = tool 'sonarqube-scanner' 
        }
        steps {
            withSonarQubeEnv(credentialsId: 'sonarqube-token', installationName: 'sonarqube-10.4.1') {
                dir("${WORKSPACE}/${APP_NAME}") {
                    sh """
                    ${SCANNER_HOME}/bin/sonar-scanner \
                    -Dsonar.projectKey=demo-${APP_NAME} \
                    -Dsonar.projectName=demo-${APP_NAME} \
                    -Dsonar.sources=. \
                    -Dsonar.exclusions=**/build/**,**/test/** \
                    -Dsonar.java.binaries=target/classes \
                    -Dsonar.sourceEncoding=UTF-8 \
                    -Dsonar.projectVersion=version-${BUILD_NUMBER}
                    """
                }
            }
        }
    }


        stage('Build Docker Image') {
            steps {
                script {
                    sh """
                    docker build -t ${DOCKER_HUB_REPO}:${env.BUILD_NUMBER} .
                    """
                }
            }
        }
 
        stage('Push Docker Image') {
            steps {
                script {
                     withCredentials([usernamePassword(credentialsId: 'docker-hub-dodol', passwordVariable: 'password', usernameVariable: 'username')]) {
                        sh """
                        docker login -u ${username} -p ${password}
                        docker push ${DOCKER_HUB_REPO}:${env.BUILD_NUMBER}
                        """
                    }
                }
            }
        }

        stage('Deploy to Server') {
            steps {
                script {
                    sh """
                    docker rm -f ${APP_NAME} && \
                    docker run -d --name ${APP_NAME} \
                    -p ${PUBLISH_PORT}:8000 \
                    ${DOCKER_HUB_REPO}:${env.BUILD_NUMBER}
                    """               
                }
            }
        } 
    }
    post {
      failure {
        echo "finish"
      }
      always {
        sh "docker system prune -af"
        cleanWs()
      }
    }
}
